<th class="tb-sort" scope="col" data-sort="{{ $column }}" data-order="<?php if(isset($sort[$column])) echo $nextOrder[$sort[$column]]; else echo 'asc'; ?>"><a href="#">{{ $label }}</a>&nbsp;
	@if(isset($sort[$column]) && $sort[$column] == 'asc')
		<i class="fas fa-sort-amount-up"></i>
	@elseif(isset($sort[$column]) && $sort[$column] == 'desc')
		<i class="fas fa-sort-amount-down"></i>
	@endif
</th>