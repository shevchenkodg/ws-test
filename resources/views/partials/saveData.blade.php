@section('scripts')

    <script>
        $('#form').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: '{{ '/'.\Request::segment(1).'/'.\Request::segment(2) }}@if(\Request::segment(3)){{ '/'.\Request::segment(3) }}@endif',
                type: 'post',
                dataType: 'json',
                data: $('#form').serialize(),
                success: function (data) {
                    if (data.redirect) {
                        window.location.replace('//' + data.redirect);
                    }
                },
                error: function (xhr) {
                    if (xhr.responseJSON !== undefined) {
                        if (xhr.responseJSON.errors !== undefined) {
                            for (var key in xhr.responseJSON.errors) {
                                $('#' + key + 'Danger').text(xhr.responseJSON.errors[key][0]);
                            }
                        }
                    }
                }
            });
        });
    </script>

@endsection