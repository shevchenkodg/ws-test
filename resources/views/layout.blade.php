<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @if(!empty($title))
        <title>{{ $title }}</title>
    @endif
    <link rel="stylesheet" href="/template/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/fa-svg-with-js.css">
</head>
<body>

<div class="wrapper">
    <div class="container">
        <h1>{{ $title }}</h1>
    </div>
</div>
<br>

@yield('main')

<script src="/template/js/jquery.min.js"></script>
<script src="/template/js/bootstrap/bootstrap.min.js"></script>
<script src="/template/js/fontawesome/fontawesome-all.min.js"></script>

@section('scripts')
@show

</body>
</html>