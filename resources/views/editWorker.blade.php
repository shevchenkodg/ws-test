@extends('layout')

@section('main')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {!! Form::open(['id' => 'form']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Имя'); !!}
                        {!! Form::text('name', $worker->name, ['class' => 'form-control']); !!}
                        <small id="nameDanger" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        {!! Form::label('surname', 'Фамилия'); !!}
                        {!! Form::text('surname', $worker->surname, ['class' => 'form-control']); !!}
                        <small id="surnameDanger" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        {!! Form::label('salary', 'Должность'); !!}
                        {!! Form::select('position_id', $positions, $worker->position_id, ['class' => 'custom-select']) !!}
                        <small id="position_idDanger" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        {!! Form::label('characteristic', 'Характеристика'); !!}
                        {!! Form::textarea('characteristic', $worker->characteristic, ['class' => 'form-control']); !!}
                        <small id="characteristicDanger" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        {!! Form::label('salary', 'Зарплата'); !!}
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Eur</span>
                            </div>
                            {!! Form::text('salary', $worker->salary, ['class' => 'form-control']); !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']); !!}
                    </div>
                    {!! Form::close(); !!}
                    <br>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('partials.saveData');