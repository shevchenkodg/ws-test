@extends('layout')

@section('main')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('partials.messages')
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="clearfix">
                        <div class="float-left">
                            <a href="/positions/add" class="btn btn-primary">Добавить должность</a>
                        </div>
                        <div class="float-right">
                            <a href="/workers" class="btn btn-secondary">Работники</a>
                        </div>
                    </div>
                    <br>
                </div>
            </div>

            @if(count($positions))

                <div class="row">
                    <div class="col-lg-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Название должности</th>
                                <th scope="col">Описание должности</th>
                                <th scope="col" width="20%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($positions as $position)
                                <tr>
                                    <td>{{ $position->name }}</td>
                                    <td>{{ $position->description }}</td>
                                    <td>
                                        <div><a href="/positions/edit/{{ $position->id }}" class="btn btn-primary">Редактировать</a></div>
                                        <br>
                                        <div><a href="/positions/delete/{{ $position->id }}" class="btn btn-danger">Удалить</a></div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        <div>
                            {!! $positions->render() !!}
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-primary" role="alert">Должности еще не были добавлены.</div>
            @endif


        </div>
    </div>

@endsection