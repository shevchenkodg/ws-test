@extends('layout')

@section('main')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {!! Form::open(['id' => 'form']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Название'); !!}
                        {!! Form::text('name', $position->name, ['class' => 'form-control']); !!}
                        <small id="nameDanger" class="form-text text-danger"></small>
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Описание'); !!}
                        {!! Form::textarea('description', $position->description, ['class' => 'form-control']); !!}
                        <small id="descriptionDanger" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']); !!}
                    </div>
                    {!! Form::close(); !!}
                    <br>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('partials.saveData');