<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	'accepted'             => ':attribute должен быть принят.',
	'active_url'           => ':attribute не является допустимым URL.',
	'after'                => ':attribute должен быть датой после :date.',
	'alpha'                => ':attribute может содержать только буквы.',
	'alpha_dash'           => ':attribute может содержать только буквы, цифры и дефис.',
	'alpha_num'            => ':attribute может содержать только буквы и цифры.',
	'array'                => ':attribute должен быть массивом..',
	'before'               => ':attribute должен быть датой раньше :date.',
	'between'              => [
		'numeric' => ':attribute должен находиться в пределах :min и :max.',
		'file'    => ':attribute должен быть от :min до :max килобайт.',
		'string'  => ':attribute должен быть от :min до :max символов.',
		'array'   => ':attribute должен иметь от :min до :max пунктов.',
	],
	'boolean'              => 'Поле :attribute должно быть истинным или ложным.',
	'confirmed'            => 'Подтверждение поля :attribute не совпадает со значением.',
	'date'                 => ':attribute не является допустимой датой.',
	'date_format'          => ':attribute не соответствует формату :format.',
	'different'            => ':attribute и :other должны быть разными.',
	'digits'               => ':attribute должен состоять из :digits цифр.',
	'digits_between'       => ':attribute должен быть от :min до :max цифр.',
	'distinct'             => 'Поле :attribute имеет повторяющееся значение.',
	'email'                => ':attribute должен быть действительным адресом электронной почты.',
	'exists'               => 'Выбранный :attribute является недействительным.',
	'filled'               => 'Поле :attribute обязательное для заполнения.',
	'image'                => ':attribute должен быть изображением.',
	'in'                   => 'Выбранный :attribute является недействительным.',
	'in_array'             => 'Поле :attribute не существует в :other.',
	'integer'              => ':attribute должен быть целым числом.',
	'ip'                   => ':attribute должен быть действительным IP-адресом.',
	'json'                 => ':attribute должен быть правильной строкой JSON.',
	'max'                  => [
		'numeric' => ':attribute не может быть больше :max.',
		'file'    => ':attribute не может быть больше :max килобайт.',
		'string'  => ':attribute не может быть больше :max символов.',
		'array'   => ':attribute не может иметь больше :max элементов.',
	],
	'mimes'                => ':attribute должен быть файлом типа: :values.',
	'min'                  => [
		'numeric' => ':attribute должен быть не менее :min.',
		'file'    => ':attribute должен быть не менее :min килобайт.',
		'string'  => ':attribute должен быть не менее :min символов.',
		'array'   => ':attribute должен быть не менее :min пунктов.',
	],
	'not_in'               => 'Выбранный :attribute является недействительным.',
	'numeric'              => ':attribute должен быть числом.',
	'present'              => 'Поле :attribute должно присутствовать.',
	'regex'                => 'Формат :attribute является недействительным.',
	'required'             => 'Поле :attribute обязательное для заполнения.',
	'required_if'          => 'Поле :attribute обязательное для заполнения, когда :other равно :value.',
	'required_unless'      => 'Поле :attribute обязательное для заполнения пока :other нет в in :values.',
	'required_with'        => 'Поле :attribute обязательное для заполнения, когда есть :values.',
	'required_with_all'    => 'Поле :attribute обязательное для заполнения, когда есть :values.',
	'required_without'     => 'Поле :attribute обязательное для заполнения, когда нет :values.',
	'required_without_all' => 'Поле :attribute обязательное для заполнения, когда ни одного из :values нет.',
	'same'                 => ':attribute и :other должны совпадать.',
	'size'                 => [
		'numeric' => ':attribute должен быть :size.',
		'file'    => ':attribute должен быть :size килобайт.',
		'string'  => ':attribute должен быть :size символов.',
		'array'   => ':attribute должен состаять из :size элементов.',
	],
	'string'               => ':attribute должен быть строкой.',
	'timezone'             => ':attribute должен быть действительной зоной.',
	'unique'               => ':attribute уже занят.',
	'url'                  => 'Формат :attribute является недействительным.',

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [
		'name'                 => 'Имя',
		'surname'              => 'Фамилия',
		'position_id'          => 'Должность',
	],

];
