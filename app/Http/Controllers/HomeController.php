<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getIndex() {

    	$title = 'Выберите раздел';

    	return view('home', compact(['title']));
    }
}
