<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('position_id')->unsigned();
	        $table->char('name', 255);
	        $table->char('surname', 255);
	        $table->text('characteristic')->nullable();
	        $table->decimal('salary', 10, 2)->nullable();
	        $table->enum('salary_currency', ['eur'])->default('eur');
            $table->timestamps();
	        $table->foreign('position_id')->references('id')->on('positions')->onUpdate('no action')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
