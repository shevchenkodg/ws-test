<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', 'HomeController@getIndex');

// Positions
Route::get('/positions', 'PositionsController@getIndex');
//Route::post('/positions', 'PositionsController@postIndex');
Route::get('/positions/add', 'PositionsController@getAdd');
Route::post('/positions/add', 'PositionsController@postAdd');
Route::get('/positions/edit/{id}', 'PositionsController@getEdit');
Route::post('/positions/edit/{id}', 'PositionsController@postEdit');
Route::get('/positions/delete/{id}', 'PositionsController@getDelete');

// Workers
Route::get('/workers', 'WorkersController@getIndex');
Route::get('/workers/add', 'WorkersController@getAdd');
Route::post('/workers/add', 'WorkersController@postAdd');
Route::get('/workers/edit/{id}', 'WorkersController@getEdit');
Route::post('/workers/edit/{id}', 'WorkersController@postEdit');
Route::get('/workers/delete/{id}', 'WorkersController@getDelete');